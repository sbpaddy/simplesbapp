package com.example.simplesbapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimplesbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimplesbappApplication.class, args);
	}

}
